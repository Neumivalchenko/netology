package Games;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class FieldOfMiracles {

    static String word = "FirstJava";
    static char[] chars = word.toCharArray();


    public static void fieldOfMiraclesStart () throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        char[] maskWord = new char[word.length()];
        Arrays.fill(maskWord, '-');

        System.out.println("Привет, угадай слово вводя по одной букве!\n" + String.valueOf(maskWord));
        System.out.println("Чтобы остановить игру - введи: 'stop'");

        while(!(String.valueOf(maskWord).equals(word))){
            String temp = reader.readLine();
            if(word.contains(temp.toLowerCase()) || word.contains(temp.toUpperCase())){
                for(int i = 0; i < chars.length; i++){
                    if (chars[i] == temp.toLowerCase().charAt(0) || chars[i] == (temp.toUpperCase()).charAt(0)) {
                        maskWord[i] = chars[i];
                    }
                }
                System.out.println("Удача!\n" + String.valueOf(maskWord));
            } else if(temp.equals("stop")) {
                System.out.println("Игра окончена!");
                break;
            } else System.out.println("Промах! Попробуй ввести еще!");
        }
        if(word.equals(String.valueOf(maskWord))) {
            System.out.println("Поздравляю с победой!");
            System.out.println("Начинаю новую игру!");
        } else System.out.println("Скоро увидимся!");
    }


}
